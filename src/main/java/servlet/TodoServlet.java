package servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import dto.Error;
import dto.Todo;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet(
        name = "TodoList",
        urlPatterns = {"/todo"}
)
public class TodoServlet extends HttpServlet {

    private final Map<Integer, String> todo = new HashMap<>();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private int counter = 1;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        try {
            List<Todo> todoList = todo.entrySet().stream()
                    .map(e -> new Todo(e.getKey(), e.getValue()))
                    .collect(Collectors.toList());
            sendResponse(resp, todoList);
        } catch (Exception e) {
            resp.setStatus(500);
            sendResponse(resp, new Error("Service error"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        try {
            Todo requestBody = objectMapper.readValue(req.getReader(), Todo.class);
            if (requestBody.getText() == null || requestBody.getText().isEmpty()) {
                resp.setStatus(400);
                sendResponse(resp, new Error("Input text is empty"));
                return;
            }

            if (requestBody.getId() != null && !todo.containsKey(requestBody.getId())) {
                resp.setStatus(404);
                sendResponse(resp, new Error("Not Found by key: " + requestBody.getId()));
                return;
            }
            int key = requestBody.getId() == null
                    ? counter++
                    : requestBody.getId();
            todo.put(key, requestBody.getText());
            sendResponse(resp, new Todo(key, requestBody.getText()));
        } catch (Exception e) {
            resp.setStatus(500);
            sendResponse(resp, new Error("Service error"));
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) {
        try {
            if (req.getParameter("id") == null) {
                todo.clear();
                return;
            }
            Integer id = Integer.valueOf(req.getParameter("id"));
            if (todo.remove(id) == null) {
                resp.setStatus(404);
                sendResponse(resp, new Error("Not Found by key: " + id));
            }
        } catch (Exception e) {
            resp.setStatus(500);
            sendResponse(resp, new Error("Service error"));
        }
    }

    private void sendResponse(HttpServletResponse resp, Object object) {
        PrintWriter out = null;
        ObjectWriter ow;
        try {
            resp.setContentType("application/json");
            ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String response = ow.writeValueAsString(object);
            out = resp.getWriter();
            out.print(response);
            out.flush();
        } catch (Exception e) {
            resp.setStatus(500);
            if (out != null) {
                String res = new Error(e.getMessage()).toJSON();
                out.write(res);
                out.flush();
            }
        }
    }
}

package dto;

public class Error {

    private String error;

    public Error(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "Error{" +
                "error='" + error + '\'' +
                '}';
    }

    public String toJSON() {
        return "{" +
                "\"error\":\"" + error + '\"' +
                '}';
    }
}
